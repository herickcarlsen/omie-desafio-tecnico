package com.example.omie.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.omie.databinding.ItemVhBinding
import com.example.omie.model.OrderItem

class OrderAdapter(
    private val dataList: List<OrderItem>,
    private val deleteOrder: (OrderItem) -> Unit,
) : RecyclerView.Adapter<OrderAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemVhBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = dataList[position]
        holder.bind(data, position)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    private fun removeItem(position: Int) {
        deleteOrder(dataList[position])
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, itemCount)
    }

    inner class ViewHolder(private val binding: ItemVhBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(data: OrderItem, position: Int) {
            binding.itemName.text = data.itemName
            binding.itemDescription.text = data.itemDescription
            binding.itemQuantity.text = data.itemQuantity.toString()
            binding.itemPrice.text = "$CURRENCY ${data.itemPrice}"
            binding.itemTotal.text = "$CURRENCY ${data.itemTotal}"
            binding.itemDelete.setOnClickListener {
                removeItem(position)
            }
        }
    }

    companion object {
        private const val CURRENCY = "R$:"
    }

}
