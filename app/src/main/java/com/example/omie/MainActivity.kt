package com.example.omie

import android.app.Dialog
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.omie.adapter.OrderAdapter
import com.example.omie.databinding.ActivityMainBinding
import com.example.omie.databinding.OrderDialogBinding
import com.example.omie.model.OrderItem
import com.example.omie.viewModel.OrderViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private val viewModel : OrderViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        handleObservables()
        setListener()
    }

    private fun handleObservables() {
        viewModel.orders.observe(this) {
            setAdapter(it)
        }
        viewModel.totalPrice.observe(this) {
            binding.totalPrice.text = "R$: $it"
        }
    }

    private fun setAdapter(orderItems: List<OrderItem>) {
        val orderAdapter = OrderAdapter(orderItems) { orderItem ->
            viewModel.deleteOrder(orderItem)
        }
        binding.adapter.adapter = orderAdapter
        binding.adapter.layoutManager = LinearLayoutManager(this)
    }

    private fun setListener() {
        binding.btnAddOrder.setOnClickListener {
            openDialog()
        }
    }

    private fun openDialog() {
        val dialog = Dialog(this)
        val binding = OrderDialogBinding.inflate(layoutInflater)

        dialog.setContentView(binding.root)

        binding.idBtnCancel.setOnClickListener {
            dialog.dismiss()
        }

        binding.idBtnAdd.setOnClickListener {
            val itemName: String = binding.idEdItemName.text.toString()
            val description: String = binding.idEdItemDescription.text.toString()
            val itemQuantity: String = binding.idEdItemQuantity.text.toString()
            val itemPrice: String = binding.idEdItemPrice.text.toString()

            val qty: Int = if (itemQuantity.isNotEmpty()) itemQuantity.toInt() else 0
            val pr: Double = if (itemPrice.isNotEmpty()) itemPrice.toDouble() else 0.0

            viewModel.addOrder(
                OrderItem(
                    itemName = itemName,
                    itemDescription = description,
                    itemQuantity = qty,
                    itemPrice = pr,
                    itemTotal = qty * pr
                )
            )

            Toast.makeText(applicationContext, "Produto Adicionado..", Toast.LENGTH_SHORT)
                .show()

            dialog.dismiss()
        }
        dialog.show()
    }
}
