package com.example.omie.repository

import com.example.omie.database.OrderItemEntity

interface OrderItemRepository {
    suspend fun getAllOrderItems(): List<OrderItemEntity>
    suspend fun insertOrderItem(orderItem: OrderItemEntity)
    suspend fun removeOrderItem(orderItem: OrderItemEntity)
}