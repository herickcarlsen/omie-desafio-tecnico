package com.example.omie.repository

import com.example.omie.database.OrderItemDao
import com.example.omie.database.OrderItemEntity

class OrderItemRepositoryImpl(private val orderItemDao: OrderItemDao) : OrderItemRepository {
    override suspend fun getAllOrderItems(): List<OrderItemEntity> {
        return orderItemDao.getAllOrderItems()
    }

    override suspend fun insertOrderItem(orderItem: OrderItemEntity) {
        orderItemDao.insertOrderItem(orderItem)
    }

    override suspend fun removeOrderItem(orderItem: OrderItemEntity) {
        orderItemDao.removeOrderItem(orderItem)
    }
}