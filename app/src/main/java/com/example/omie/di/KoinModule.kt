package com.example.omie.di

import androidx.room.Room
import com.example.omie.database.OrderItemDatabase
import com.example.omie.repository.OrderItemRepository
import com.example.omie.repository.OrderItemRepositoryImpl
import com.example.omie.viewModel.OrderViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

class KoinModule {
    val appModule = module {
        viewModel { OrderViewModel(get()) }

        single {
            Room.databaseBuilder(
                androidContext(),
                OrderItemDatabase::class.java, "order_item_database"
            ).build()
        }

        single { get<OrderItemDatabase>().orderItemDao() }

        single<OrderItemRepository> { OrderItemRepositoryImpl(get()) }

       // factory { OrderDialog(context = androidContext(), viewModel = get()) }
    }
}