package com.example.omie.model

data class OrderItem(
    val id: Long = 0,
    val itemDescription: String,
    val itemName: String,
    val itemQuantity: Int,
    val itemPrice: Double,
    val itemTotal: Double
)
