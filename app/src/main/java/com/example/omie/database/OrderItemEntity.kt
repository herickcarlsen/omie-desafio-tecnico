package com.example.omie.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "order_items")
data class OrderItemEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val itemName: String,
    val itemDescription: String,
    val itemQuantity: Int,
    val itemPrice: Double,
    val itemTotal: Double
)