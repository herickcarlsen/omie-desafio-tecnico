package com.example.omie.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface OrderItemDao {
    @Query("SELECT * FROM order_items")
    fun getAllOrderItems(): List<OrderItemEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderItem(orderItem: OrderItemEntity)

    @Delete()
    fun removeOrderItem(orderItem: OrderItemEntity)
}