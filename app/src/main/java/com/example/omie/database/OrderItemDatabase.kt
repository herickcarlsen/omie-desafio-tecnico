package com.example.omie.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [OrderItemEntity::class], version = 1)
abstract class OrderItemDatabase : RoomDatabase() {
    abstract fun orderItemDao(): OrderItemDao
}