package com.example.omie.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.omie.database.OrderItemEntity
import com.example.omie.model.OrderItem
import com.example.omie.repository.OrderItemRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class OrderViewModel(private val repository: OrderItemRepository) : ViewModel() {

    private val _orders = MutableLiveData<List<OrderItem>>()
    val orders: LiveData<List<OrderItem>> = _orders

    private val _totalPrice = MutableLiveData<Double>()
    val totalPrice: LiveData<Double> = _totalPrice

    init {
        fetchOrderList()
    }

    fun addOrder(order: OrderItem) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insertOrderItem(order.toEntityOrder())
            fetchOrderList()
        }
    }

    fun deleteOrder(order: OrderItem) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.removeOrderItem(order.toEntityOrder())
            fetchOrderList()
        }
    }

    private fun fetchOrderList() {
        viewModelScope.launch(Dispatchers.IO) {
            val featureOrders = repository.getAllOrderItems().map {
                it.toFeatureOrder()
            }
            val total = featureOrders.sumOf { it.itemTotal }

            _orders.postValue(featureOrders)
            _totalPrice.postValue(total)
        }
    }

    private fun OrderItemEntity.toFeatureOrder(): OrderItem {
        return OrderItem(
            id = this.id,
            itemName = this.itemName,
            itemDescription = this.itemDescription,
            itemQuantity = this.itemQuantity,
            itemPrice = this.itemPrice,
            itemTotal = this.itemTotal
        )
    }

    private fun OrderItem.toEntityOrder(): OrderItemEntity {
        return OrderItemEntity(
            id = this.id,
            itemName = this.itemName,
            itemDescription = this.itemDescription,
            itemQuantity = this.itemQuantity,
            itemPrice = this.itemPrice,
            itemTotal = this.itemTotal
        )
    }
}