# Desafio Técnico Omie

-Kotlin

-LiveData

-MVVM

-ROOM

-KOIN

-View Binding

## APK: [Download the APK file](app-debug.apk)

## DEMO:

![omie-demo](omie.gif)
